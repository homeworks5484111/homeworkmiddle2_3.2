// Homework2_3.2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <iterator>
#include <cstddef> 
#include <vector>

template <typename PointerType>
class Iterator
{
    // Tags - for better perfomance!
    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;

    // Constructor
    Iterator(PointerType* ptr) : ptr(ptr) {}

    // Operators implementation
    PointerType& operator*() const { return *ptr; }
    PointerType* operator->() { return ptr; }
    Iterator& operator++() { ptr++; return *this; } // Prefix increment
    Iterator operator++(PointerType) { Iterator tmp = *this; ++(*this); return tmp; } // Postfix increment

    friend bool operator== (const Iterator& a, const Iterator& b) { return a.ptr == b.ptr; };
    friend bool operator!= (const Iterator& a, const Iterator& b) { return a.ptr != b.ptr; };

private:

    PointerType* ptr;
};

int main()
{
    std::vector<int> MyVector = {4, 6, 78, 456, 10, 0};
    for (auto it = MyVector.begin(), end = MyVector.end(); it != end; ++it) {
        const auto i = *it;
        std::cout << i << "\n";
    }
}
